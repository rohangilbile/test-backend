FROM node:14
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
#RUN apk update
#RUN apk add git
RUN npm install
COPY . /app
EXPOSE 3000
CMD [ "node", "index.js" ]